(in-package :cl-user)
(defpackage tools
  (:use :cl)
  (:export :empty-string-p :parse-file-into-list :abspath :parse-file-into-list-by-blank-line)
  )
(in-package :tools)


(defun empty-string-p (string)
  "Return true if the STRING is empty or nil. Expects string type."
  (or (null string)
      (zerop (length string))))


(defun parse-file-into-list (filename)
  "Parse a file into a list, splitting by newlines"
  (let ((input-list (list)))
    (with-open-file (stream filename)
      (do ((line (read-line stream nil)
                 (read-line stream nil)))
          ((null line))
        (unless (empty-string-p line)
          (setf input-list
                (append input-list
                        (list line))))))
    (return-from parse-file-into-list input-list)))


(defun parse-file-into-list-by-blank-line (filename)
  "Parse a file into a list, splitting on blank lines"
  (let ((group (list))
        (result (list)))
    (with-open-file (stream filename)
      (do ((line (read-line stream nil)
                 (read-line stream nil)))
          ((null line))
        (if (empty-string-p line)
            (progn
              (push group result)
              (setf group (list)))
            (push line group))))
    (push group result) ; do the last one
    (return-from parse-file-into-list-by-blank-line result)))


(defun abspath (pathname)
  (merge-pathnames pathname *default-pathname-defaults*))


(defun count-matches (regex string)
  "Return the number of matches"
  (let ((match-index-count (length (ppcre:all-matches regex string))))
    (if (< 0 match-index-count)
        (/ 2 match-index-count)
        0)))
