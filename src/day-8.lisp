(in-package :cl-user)
(defpackage aoc2020/day-8
  (:use :cl :str)
  (:import-from :tools :parse-file-into-list :abspath :count-matches :parse-file-into-list-by-blank-line)
  (:import-from :alexandria )
  (:export :day-8)
  )
(in-package :aoc2020/day-8)


(defparameter *input-file* nil "File where the raw program is found")
(defvar *program* nil "The parsed program listing")
(defvar *next-op-id* 0 "A unique number for the op struct id")

(defstruct op
  id
  type
  argument)


(defun build-operation (type argument)
  "Create an op struct from an op type and argument"
  (make-op :type type :argument argument :id (incf *next-op-id*)))


(defun setup ()
  (if (null *input-file*)
      (setf *input-file* (abspath "../inputs/day-8.txt")))
  (setf *next-op-id* 0)
  (setf *program*
        (mapcar (lambda (x) (apply 'build-operation (split " " x)))
                 (parse-file-into-list *input-file*))))


(defun run (program)
  "Execute the sequential operations making up the program"
  (let ((instruction (first program))
        (accumulator 0)
        (counter 0)
        (exit-code 0)
        (operations-run (list)))
    (loop
      (if (instruction-overrun-p counter (length program))
          (progn
            (setf exit-code 0)
            (return)))
      (if (instruction-repeated-p counter operations-run)
          (progn
            (setf exit-code 1)
            (return)))
      (push counter operations-run)
      (cond ((string= "acc" (op-type instruction))
             (incf accumulator (parse-integer (op-argument instruction)))
             (incf counter))
            ((string= "jmp" (op-type instruction))
             (incf counter (parse-integer (op-argument instruction))))
            ((string= "nop" (op-type instruction))
             (incf counter)))
      (setf instruction (nth counter program)))
    (values exit-code accumulator)))


(defun instruction-repeated-p (counter history)
  "Returns true if the instruction has been repeated"
  (member counter history))


(defun instruction-overrun-p (counter program-length)
  "Returns true if the counter overruns the program length"
  (>= counter program-length))


(defun part-1 ()
  "The first part"
  (multiple-value-bind (exit-code accumulator)
      (run *program*)
    (if (= exit-code 1)
        (print "program loops forever"))
    (if (= exit-code 0)
        (print "program execution reached end"))
    accumulator))


(defun part-2 ()
  "The second part"
  (let ((instructions
          (remove-if (lambda (x) (string= (op-type x) "acc")) *program*))
        (modified-program))
    (dolist (instruction instructions)
      (setf (op-type instruction)
            (if (string= "nop" (op-type instruction))
                "jmp"
                "nop"))
      (multiple-value-bind (exit-code accumulator) (run *program*)
        (if (= exit-code 0)
            (progn
              (print "ok")
              (print accumulator)
              (return)))
      (setf (op-type instruction)
            (if (string= "nop" (op-type instruction))
                "jmp"
                "nop"))))))


(defun day-8 ()
  "The eighth day"
  (setup)
  (print (part-1))
  (part-2))
