(in-package :cl-user)
(defpackage aoc2020/day-4
  (:use :cl :str)
  (:import-from :tools :parse-file-into-list :abspath :count-matches :parse-file-into-list-by-blank-line)
  (:import-from :alexandria )
  (:export :day-4)
  )
(in-package :aoc2020/day-4)

(defparameter *input-file* nil)
(defparameter *passports* nil)
(defparameter *required-fields* (sort (list "byr" "iyr" "eyr" "hgt" "hcl" "ecl" "pid") #'string-lessp))
;    byr ;(Birth Year)
;    iyr ;(Issue Year)
;    eyr ;(Expiration Year)
;    hgt ;(Height)
;    hcl ;(Hair Color)
;    ecl ;(Eye Color)
;    pid ;(Passport ID)
;    ci) ;(Country ID)


(defun fields-present-p (passport)
  "Returns true if a passport has all required fields"
  (let* ((all-keys (sort
               (alexandria:hash-table-keys passport)
               #'string-lessp))
         (keys (remove-if (lambda(x) (string= x "cid")) all-keys)))
    (equalp keys *required-fields*)))


(defun fields-valid-p (passport)
  "Returns true if the fields present are valid"
  (and
   (not 0 (count-matches "\d{4}" (gethash "byr" passport)))

   )
  ; byr (Birth Year) - four digits; at least 1920 and at most 2002.
  ; iyr (Issue Year) - four digits; at least 2010 and at most 2020.
  ; eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
  ; hgt (Height) - a number followed by either cm or in:
  ;
  ; If cm, the number must be at least 150 and at most 193.
  ; If in, the number must be at least 59 and at most 76.
  ;
  ; hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
  ; ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
  ; pid (Passport ID) - a nine-digit number, including leading zeroes.
  )


(defun set-inputs ()
  (if (null *input-file*)
      (setf *input-file* (abspath "../inputs/day-4.txt"))))


(defun passport-from-text (text)
  "Build a passport object from a text record"
  (let ((attributes (str:split " " text))
        (attr-name)
        (attr-value)
        (attr-pair)
        (passport-instance (make-hash-table :test 'equal)))
    (dolist (attribute-pair attributes)
      (setq attr-pair (str:split ":" attribute-pair))
      (setf attr-name (pop attr-pair))
      (setf attr-value (pop attr-pair))
      (setf (gethash attr-name passport-instance) attr-value))
    passport-instance))


(defun passport-texts ()
  "Build a list of passport objects"
  (set-inputs)
  (let ((raw (parse-file-into-list-by-blank-line *input-file*))
        (result (list)))
    (dolist (items raw)
      (push (str:join " " items) result))
    result))


(defun part-1 ()
  "The first part"
  (count-if (lambda (x) (not (null x)))
            (map 'list #'fields-present-p *passports*)))


(defun part-2 ()
  "The second part"
  (let ((full-passports (remove-if-not #'fields-present-p *passports*)))
    (count-if (lambda (x) (not (null x)))
             (map 'list #'fields-valid-p full-passports))))



(defun day-4 ()
  "The fourth day"
  (if (null *passports*)
      (dolist (text (passport-texts))
        (push (passport-from-text text) *passports*)))

  ;(part-1))
  (part-2))
