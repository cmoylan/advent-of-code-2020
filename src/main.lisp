(in-package :cl-user)
(defpackage aoc2020
  (:use :cl)
  (:import-from :aoc2020/day-1 :day-1)
  (:import-from :aoc2020/day-2 :day-2)
  (:import-from :aoc2020/day-3 :day-3)
  (:import-from :aoc2020/day-4 :day-4)
  (:import-from :aoc2020/day-8 :day-8)
  (:export :run))
(in-package :aoc2020)


(defun run()
  "Do it"
  ;(day-1)
  ;(day-2)
  ;(day-3)
  ;(day-4)
  (day-8)
  )
