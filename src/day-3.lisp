(in-package :cl-user)
(defpackage aoc2020/day-3
  (:use :cl :iterate)
  (:import-from :tools :parse-file-into-list :abspath :count-matches)
  (:export :day-3)
  )
(in-package :aoc2020/day-3)

(defvar *input-file* nil)
(defvar *terrain* nil)
(defparameter *terrain-width* nil)


(defun set-terrain ()
  (setf *input-file* (abspath "../inputs/day-3.txt"))
  (setf *terrain* (parse-file-into-list *input-file*))
  (setf *terrain-width* (length (car *terrain*))))


(defun value-at-coord (x y)
  "Returns the value at a coord. Will tile the map along the x-axis."
  (if (> x (- *terrain-width* 1))
      (setf x (mod x *terrain-width*)))
  (char (nth y *terrain*) x))


(defun trees-on-slope (&key slope-x slope-y)
  "Count the trees on a given slope"
  (let ((terrain-height (length *terrain*))
        (x 0)
        (y 0)
        (tree-count 0))
    (loop
      (if (> y (- terrain-height 2))
          (return))
      (setf x (+ x slope-x))
      (setf y (+ y slope-y))
      (if (char= #\# (value-at-coord x y))
          (incf tree-count)))
    tree-count))


(defun part-1 ()
  (trees-on-slope :slope-x 3
                  :slope-y 1))


(defvar *part-2-slopes*
  '((1 1)
    (3 1)
    (5 1)
    (7 1)
    (1 2)))


(defun part-2 ()
  "The second part of the third day"
  (let ((results 0))
    (reduce #'*
            (iterate (for slope-pair in *part-2-slopes*)
              (collect (trees-on-slope :slope-x (car slope-pair) :slope-y (car (last slope-pair))))))))


(defun day-3 ()
  "The third day"
  (set-terrain)
  (part-1)
  (part-2))
