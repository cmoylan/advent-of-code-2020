(in-package :cl-user)
(defpackage aoc2020/day-1
  (:use :cl :iterate)
  (:import-from :tools :parse-file-into-list :abspath)
  (:export :day-1))
(in-package :aoc2020/day-1)


(defun parse-expense-list (filename)
  (map 'list #'parse-integer
       (parse-file-into-list filename)))


(defun part-1 ()
  "Find the product of the 2 numbers whose sum is 2020 from a list of expenses"
  (let ((expense-list (parse-expense-list (abspath "../inputs/day-1.txt")))
        (results (list)))

    (iterate (for x in expense-list)
             (iterate (for y in expense-list)
                      (print (+ x y))
                      (if (= 2020 (+ x y))
                          (progn
                            (setf results (list x y))
                            (finish)))))
    (apply #'* results)))


(defun part-2 ()
  "Find the product of the 3 numbers whose sum is 2020 from a list of expenses"
  (let ((expense-list (parse-expense-list (abspath "../inputs/day-1.txt")))
        (results (list)))
    (iterate (for x in expense-list)
             (iterate (for y in expense-list)
                      (iterate (for z in expense-list)
                               (if (= 2020 (+ x y z))
                                   (progn
                                     (setf results (list x z y))
                                     (finish))))))
    (apply #'* results)))


(defun day-1 ()
  (part-1)
  (part-2)
  )
