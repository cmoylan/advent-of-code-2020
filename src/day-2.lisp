(in-package :cl-user)
(defpackage aoc2020/day-2
  (:use :cl :iterate)
  (:import-from :tools :parse-file-into-list :abspath :count-matches)
  (:export :day-2)
  )
(in-package :aoc2020/day-2)


(defstruct password-entry
  min-letter-repeat
  max-letter-repeat
  letter
  password)


(defun parse-password-entry (text)
  "Return a password-entry from text"
  (ppcre:register-groups-bind
   (min max letter password)
   ("(\\d+)-(\\d+) (\\w+): (\\w+)" text)
    (make-password-entry
     :min-letter-repeat (parse-integer min)
     :max-letter-repeat (parse-integer max)
     :letter letter
     :password password)))


(defun parse-password-database (text)
  "Parse the entire database"
  (iterate (for line in text)
    (collect (parse-password-entry line))))


(defun valid-password-p (password-entry)
  "Return true if the password has the required characters"
  (let* ((min (password-entry-min-letter-repeat password-entry))
        (max (password-entry-max-letter-repeat password-entry))
        (letter (password-entry-letter password-entry))
        (password (password-entry-password password-entry))
        (matches (count-matches letter password)))
    (and
     (<= matches max)
     (>= matches min))))


(defun day-2 ()
  "The second day"
  (let* ((inputs (parse-file-into-list (abspath "../inputs/day-2.txt")))
         (passwords (parse-password-database inputs))
        )
    (iterate (for password in passwords)
      (sum (if (valid-password-p password)
               1
               0
               ))
      (print "-----")
      (print password)
      (print (valid-password-p password))
      )
    )
  )
