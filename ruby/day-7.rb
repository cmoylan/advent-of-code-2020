#!/usr/bin/env ruby

require 'pry'

INPUT_FILE = File.join(File.dirname(__FILE__), '../inputs/day-7.txt')


class RuleSet
  Node = Struct.new(:color, :count)

  TERMINUS = "*EMPTY*".freeze

  attr_accessor :rules
  attr_reader :rule_tree
  attr_reader :search_tree

  def initialize(text_rules)
    @rules = text_rules
  end

  def build
    build_rule_tree
    build_search_tree
    self
  end

  def terminal_nodes
    @rule_tree.select{ |k,v| v == TERMINUS }
  end

  def count_contained_bags(color)
    #puts "counting #{color}"
    return 0 if @rule_tree[color] == TERMINUS

    @rule_tree[color].sum do |bag|
      puts bag
      bag_count = bag.count.to_i
      bag_count + (bag_count * count_contained_bags(bag.color).to_i)
    end
  end

  private

  def build_rule_tree
    @rule_tree = {}
    @rules.each do |rule|
      subtree = build_subtree(rule)
      @rule_tree.merge!(subtree)
    end
  end

  def build_search_tree
    @search_tree = {}
    @rule_tree.each do |root, nodes|
      next if nodes == TERMINUS

      nodes.each do |node|
        if @search_tree[node.color]
          @search_tree[node.color].append(root) unless @search_tree[node.color].include?(root)
        else
          @search_tree[node.color] = [root]
        end
      end
    end
  end

  def build_subtree(rule)
    root, branches = rule.split("\sbags\scontain\s")
    return { root => TERMINUS } if rule.match(/no other bags/)

    subtree = branches.split(", ").map do |branch|
      branch.gsub!(/bags?\.?/, '')
      /(?<count>\d+)\s(?<color>[\w\s]+)/.match(branch.strip) do |data|
        Node.new(data[:color], data[:count])
      end
    end
    { root => subtree }
  end
end

class RuleQuery
  def initialize(rule_set, color)
    @rule_set = rule_set
    @search_tree = rule_set.search_tree
    @found = []
    @color = color
  end

  def result(color = @color)
    return unless @search_tree[color]

    search_set = @search_tree[color] - @found

    search_set.map do |c|
      result(c)
      @found << c
    end
  end
end

def inputs
  File.readlines(INPUT_FILE)
#"light red bags contain 1 bright white bag, 2 muted yellow bags.
#dark orange bags contain 3 bright white bags, 4 muted yellow bags.
#bright white bags contain 1 shiny gold bag.
#muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
#shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
#dark olive bags contain 3 faded blue bags, 4 dotted black bags.
#vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
#faded blue bags contain no other bags.
#dotted black bags contain no other bags.".split("\n")

#"shiny gold bags contain 2 dark red bags.
#dark red bags contain 2 dark orange bags.
#dark orange bags contain 2 dark yellow bags.
#dark yellow bags contain 2 dark green bags.
#dark green bags contain 2 dark blue bags.
#dark blue bags contain 2 dark violet bags.
#dark violet bags contain no other bags.".split("\n")
end

def main
  rules = RuleSet.new(inputs).build
  color = "shiny gold"

  query = RuleQuery.new(rules, color).result
  query.flatten.uniq.size

  rules.count_contained_bags(color)
end

puts main
