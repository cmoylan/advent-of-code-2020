PasswordEntry = Struct.new(:min, :max, :letter, :password)

def gather_passwords
  password_regex = /^(?<min>\d+)-(?<max>\d+) (?<letter>\w): (?<password>\w+)$/
  input_file = File.join(File.dirname(__FILE__), '../inputs/day-2.txt')
  inputs = File.read(input_file)
  all = []

  inputs.each_line do |line|
    matches = password_regex.match(line)

    all << PasswordEntry.new(
      matches[:min].to_i,
      matches[:max].to_i,
      matches[:letter],
      matches[:password]
    )
  end

  all
end

def valid_password_1?(entry)
  count = entry[:password].scan(entry.letter).size
  (count >= entry[:min] &&
    count <= entry[:max])
end

def valid_password_2?(entry)
  (entry[:password][entry[:min] - 1] == entry[:letter]) ^
    (entry[:password][entry[:max] - 1] == entry[:letter])
end

def main
  password_entries = gather_passwords
  password_entries.map { |entry| valid_password_1?(entry) }.select { |a| a == true }.size
  password_entries.map { |entry| valid_password_2?(entry) }.select { |a| a == true }.size
end

puts main
