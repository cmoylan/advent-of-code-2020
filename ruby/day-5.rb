#!/usr/bin/env ruby

def row_number(seat)
  rows = Array.new(128){ |i| i }

  (0..7).each do |i|
    if seat[i] == 'F'
      #puts "#{i} - taking front"
      rows = rows[0..((rows.size / 2) - 1)]
    else
      #puts "#{i} - taking back"
      rows = rows[((rows.size / 2))..]
    end
    #puts rows.join(', ')
  end

  #puts "......"
  rows.first
end

def seat_number(seat)
  seats = Array.new(8){ |i| i }

  #puts seats.join(', ')
  (7..9).each do |i|
    #puts seat[i]
    if seat[i] == 'L'
      #puts "#{i} - taking left"
      seats = seats[0..((seats.size / 2) - 1)]
    else
      #puts "#{i} - taking right"
      seats = seats[((seats.size / 2))..]
    end
    #puts seats.join(', ')
    #puts "..."
  end

  seats.first
end

def seat_id(seat)
  (row_number(seat) * 8) + seat_number(seat)
end

def inputs
  input_file = File.join(File.dirname(__FILE__), '../inputs/day-5.txt')
  inputs = File.readlines(input_file)

  inputs
end

def main
  ticket ='FBFBBFFRLR'

  row_number(ticket)
  seat_number(ticket)
  seat_id(ticket)

  known_tickets = inputs.map{ |t| seat_id(t) }.sort
  all_ticket_ids = Array.new(951){ |i| i }
  puts all_ticket_ids.size
  puts all_ticket_ids.join(", ")
  puts '*******************************************'
  puts known_tickets.size
  puts known_tickets.join(", ")
  missing_tickets = all_ticket_ids - known_tickets
  puts '-----------------------'
  puts missing_tickets.size
  puts missing_tickets.join(", ")
  missing_tickets
end

puts main
