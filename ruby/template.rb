#!/usr/bin/env ruby

INPUT_FILE = File.join(File.dirname(__FILE__), '../inputs/day-6.txt')

def inputs
  File.readlines(INPUT_FILE)
end

def main
  inputs
end

puts main
