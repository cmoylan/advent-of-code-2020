#!/usr/bin/env ruby

INPUT_FILE = File.join(File.dirname(__FILE__), '../inputs/day-6.txt')

def inputs
  File.read(INPUT_FILE)
end

def unique_votes(group)
  (group.split("\n")).to_s.scan(/\w/).uniq.size
end

def unanimous_votes(group)
  members = group.split("\n")

  vote = members.first.scan(/\w/)
  members.each do |member|
    vote &= member.scan(/\w/)
  end
  vote.size
end

def main
  groups = inputs.split("\n\n")
  votes = groups.map{ |g| unique_votes(g) }
  votes.sum
  uncontested = groups.map{ |g| unanimous_votes(g) }
  uncontested.sum
end

puts main
