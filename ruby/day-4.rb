#!/usr/bin/env ruby

PasswordEntry = Struct.new(:min, :max, :letter, :password)

REQUIRED_FIELDS = %w[byr iyr eyr hgt hcl ecl pid].sort

def gather_passports
  # password_regex = /^(?<min>\d+)-(?<max>\d+) (?<letter>\w): (?<password>\w+)$/
  input_file = File.join(File.dirname(__FILE__), '../inputs/day-4.txt')
  inputs = File.read(input_file)
  all = []
  current_passport = {}

  inputs.each_line do |line|
    if !/\w+/.match?(line)
      all << current_passport
      current_passport = {}
    else
      pairs = line.split(' ')
      pairs.each do |pair|
        name, value = pair.split(':')
        current_passport[name] = value
      end
    end
    # matches =  password_regex.match(line)

    # all << PasswordEntry.new(
    #  matches[:min].to_i,
    #  matches[:max].to_i,
    #  matches[:letter],
    #  matches[:password]
    # )
  end
  all << current_passport

  all
end

def valid_passport?(entry)
  byr = entry['byr']
  iyr = entry['iyr']
  eyr = entry['eyr']
  hgt = entry['hgt']
  hcl = entry['hcl']
  ecl = entry['ecl']
  pid = entry['pid']
  #cid = entry['cid']

  four_digits_between?(byr, 1920, 2002) &&
    four_digits_between?(iyr, 2010, 2020) &&
    four_digits_between?(eyr, 2020, 2030) &&
    valid_hgt?(hgt) &&
    hcl.match?(/^#[0-9a-f]{6}$/) &&
    ecl.match?(/^amb|blu|brn|gry|grn|hzl|oth$/) &&
    pid.match?(/^\d{9}$/)
end

def valid_hgt?(hgt)
  return false unless hgt.match?(/^\d+(cm|in)$/)

  if (m = hgt.match(/^(\d+)cm$/))
    hgt = m[0].to_i
    return hgt >= 150 && hgt <= 193
  end
  if (m = hgt.match(/^(\d+)in$/))
    hgt = m[0].to_i
    return hgt >= 59 && hgt <= 76
  end
end

def four_digits_between?(field, low, high)
  field.match?(/^\d{4}$/) &&
    (low <= field.to_i) &&
    (field.to_i <= high)
end

def complete_passport?(entry)
  keys = entry.keys.reject { |k| k == 'cid' }
  keys.sort == REQUIRED_FIELDS
end

def main
  passport_entries = gather_passports
  complete_passports = passport_entries.select{ |p| complete_passport?(p) }
  valid_passports = complete_passports.select{ |p| valid_passport?(p) }
  [complete_passports.size, valid_passports.size ]
end

puts main
