(defsystem "aoc2020"
  :version "1.0.0"
  :author "Chris Moylan"
  :description "Advent of Code 2020"
  :depends-on (:iterate :cl-ppcre :str :alexandria)
  :components ((:module "src"
                :components
                ((:file "main" :depends-on ("day-1" "day-2" "day-3" "day-4" "day-8"))
                 (:file "tools")
                 (:file "day-1" :depends-on ("tools"))
                 (:file "day-2" :depends-on ("tools"))
                 (:file "day-3" :depends-on ("tools"))
                 (:file "day-4" :depends-on ("tools"))
                 (:file "day-8" :depends-on ("tools"))))
               )
  )
